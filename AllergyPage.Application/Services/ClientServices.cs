﻿using AllergyPage.Application.Interfaces;
using AllergyPage.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using AllergyPage.Application.Dtos;

namespace AllergyPage.Application.Services
{
    public interface IClientService
    {
        PagationDto<ClientAllergy> GetAllergyHistories(QueryDataDto query);
        void saveData(ClientAllergy clientAllergy);
    }

    enum TimeRange
    {
        All = 0,
        OneYear = 1,
        SixMonth = 2,
        Last = 3
    }

    public class ClientService : IClientService
    {
        private readonly IRepository<Client> _clientRepo;
        private readonly IRepository<ClientAllergy> _clientAllergyRepo;

        public ClientService(
            IRepository<Client> clientRepo,
            IRepository<ClientAllergy> clientAllergyRepo
            )
        {
            _clientRepo = clientRepo;
            _clientAllergyRepo = clientAllergyRepo;
        }

        public PagationDto<ClientAllergy> GetAllergyHistories(QueryDataDto queryParams)
        {
            var pageSize = queryParams.length > 10 ? queryParams.length : 10;
            var query = _clientAllergyRepo.TableNoTracking;

            if (queryParams.customs != null && queryParams.customs.ContainsKey("active"))
            {
                query = FillterActive(queryParams, query);
            }

            if (queryParams.customs != null && queryParams.customs.ContainsKey("timeRange"))
            {
                query = FillterTimeRange(queryParams, query);
            }

            string search = queryParams.search.value;
            if (!string.IsNullOrEmpty(search))
            {
                query = Search(query, search);
            }

            query = query.Skip(queryParams.start).Take(pageSize)
                .Include(_ => _.Client)
                .Include(_ => _.Allergen)
                .Include(_ => _.Reaction)
                .Include(_ => _.Severity)
                .Include(_ => _.AllergenType);

            var pagation = new PagationDto<ClientAllergy>
            {
                draw = queryParams.draw,
                recordsTotal = _clientAllergyRepo.Table.Count(),
                recordsFiltered = _clientAllergyRepo.Table.Count(),
                data = query.ToList()
            };

            return pagation;
        }

        private static IQueryable<ClientAllergy> FillterTimeRange(QueryDataDto queryParams, IQueryable<ClientAllergy> query)
        {
            TimeRange range = (TimeRange)int.Parse(queryParams.customs["timeRange"]);
            switch (range)
            {
                case TimeRange.All:
                    break;
                case TimeRange.OneYear:
                    var oneYearAgo = DateTime.UtcNow.AddDays(-365);
                    query = query.Where(_ => _.CreateDateWithTime >= oneYearAgo);
                    break;
                case TimeRange.SixMonth:
                    var sixMonthsAgo = DateTime.UtcNow.AddDays(-183);
                    query = query.Where(_ => _.CreateDateWithTime >= sixMonthsAgo);
                    break;
                case TimeRange.Last:
                    query = query.OrderByDescending(_ => _.UpdateDateWithTime.Value);
                    break;
                default:
                    break;
            }

            return query;
        }

        private static IQueryable<ClientAllergy> FillterActive(QueryDataDto queryParams, IQueryable<ClientAllergy> query)
        {
            bool isActive = queryParams.customs["active"].ToLower() == "true";
            query = query.Where(_ => _.Deleted == !isActive);

            return query;
        }

        private static IQueryable<ClientAllergy> Search(IQueryable<ClientAllergy> query, string search)
        {
            query = query.Where(_ => _.Allergen.CodeText.Contains(search)
                            || _.Allergen.CodeDesc.Contains(search)
                            || _.AllergenType.CodeDesc.Contains(search)
                            || _.AllergenType.CodeText.Contains(search)
                            || _.Client.Name.Contains(search)
                            || _.CreateUser.Contains(search)
                            || _.UpdateUser.Contains(search)
                            || _.Reaction.CodeDesc.Contains(search)
                            || _.Severity.CodeDesc.Contains(search)
                            || _.Note.Contains(search)
                            );
            return query;
        }

        public void saveData(ClientAllergy clientAllergy)
        {
            _clientAllergyRepo.Insert(clientAllergy);
        }
    }
}
