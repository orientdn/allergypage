﻿using AllergyPage.Application.Interfaces;
using AllergyPage.Domain.Entities;
using AllergyPage.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AllergyPage.Application.Services
{
    public interface IAllergenService
    {
        IList<Allergen> GetAllAllergen();

        IList<AllergenType> GetAllAllergenType();

        IList<AllergenReaction> GetAllReaction();

        IList<AllergenSeverity> GetAllSeverity();
    }

    public class AllergenService: IAllergenService
    {
        private readonly IRepository<Allergen> _allergenRepo;
        private readonly IRepository<AllergenReaction> _allergenReactiontRepo;
        private readonly IRepository<AllergenSeverity> _allergenSeveritytRepo;
        private readonly IRepository<AllergenType> _allergenTypeRepo;

        public AllergenService(
            IRepository<Allergen> allergenRepo,
            IRepository<AllergenReaction> allergenReactiontRepo,
            IRepository<AllergenSeverity> allergenSeveritytRepo,
            IRepository<AllergenType> allergenTypeRepo
            )
        {
            _allergenRepo = allergenRepo;
            _allergenReactiontRepo = allergenReactiontRepo;
            _allergenSeveritytRepo = allergenSeveritytRepo;
            _allergenTypeRepo = allergenTypeRepo;
        }

        public IList<Allergen> GetAllAllergen()
        {
            return _allergenRepo.TableNoTracking.ToList();
        }

        public IList<AllergenType> GetAllAllergenType()
        {
            return _allergenTypeRepo.TableNoTracking.ToList();
        }

        public IList<AllergenReaction> GetAllReaction()
        {
            return _allergenReactiontRepo.TableNoTracking.ToList();
        }

        public IList<AllergenSeverity> GetAllSeverity()
        {
            return _allergenSeveritytRepo.TableNoTracking.ToList();
        }
    }
}
