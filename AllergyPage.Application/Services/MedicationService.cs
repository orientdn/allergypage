﻿using AllergyPage.Application.Interfaces;
using AllergyPage.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using AllergyPage.Application.Dtos;

namespace AllergyPage.Application.Services
{
    public interface IMedicationService
    {
        List<Medication> GetMedications();
    }

    public class MedicationService: IMedicationService
    {
        private readonly IRepository<Medication> _medicationRepo;

        public MedicationService(IRepository<Medication> medicationRepo)
        {
            _medicationRepo = medicationRepo;
        }

        public List<Medication> GetMedications()
        {
            var result = _medicationRepo.TableNoTracking;
            return result.ToList();
        }
    }
}
