﻿namespace AllergyPage.Application.Dtos
{
    public class SaveDataVM
    {
        public int ClientId { get; set; }
        public int? AllergenId { get; set; }
        public int? AllergenTypeId { get; set; }
        public int? ReactionId { get; set; }
        public int? SeverityId { get; set; }
        public int? DrugId { get; set; }
        public string Note { get; set; }
    }
}
