﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AllergyPage.Application.Dtos
{
    public class PagationDto<T>
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public IList<T> data { get; set; }
    }
}
