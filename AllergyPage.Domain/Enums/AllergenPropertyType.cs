﻿namespace AllergyPage.Domain.Enums
{
    public enum AllergenPropertyType
    {
        Allergen = 62,
        AllergenReaction = 64,
        AllergenSeverity = 65,
        AllergenType = 63
    }
}
