﻿using System.ComponentModel.DataAnnotations;

namespace AllergyPage.Domain.Entities
{
    public class Medication
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string LexiDrugId { get; set; }
    }
}
