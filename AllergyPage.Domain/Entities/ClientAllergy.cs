﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AllergyPage.Domain.Entities
{
    public class ClientAllergy
    {
        [Key]
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int? AllergenId { get; set; }
        public int? AllergenTypeId { get; set; }
        public int? ReactionId { get; set; }
        public int? SeverityId { get; set; }
        public int? DrugId { get; set; }
        public string Note { get; set; }
        public bool Deleted { get; set; }
        public string CreateUser { get; set; }
        public string UpdateUser { get; set; }
        public DateTime CreateDateWithTime { get; set; }
        public DateTime? UpdateDateWithTime { get; set; }

        [ForeignKey("ClientId")]
        public virtual Client Client { get; set; }
        [ForeignKey("AllergenId")]
        public virtual Allergen Allergen { get; set; }
        [ForeignKey("AllergenTypeId")]
        public virtual AllergenType AllergenType { get; set; }
        [ForeignKey("ReactionId")]
        public virtual AllergenReaction Reaction { get; set; }
        [ForeignKey("SeverityId")]
        public virtual AllergenSeverity Severity { get; set; }
        [ForeignKey("DrugId")]
        public virtual Medication Drug { get; set; }
    }
}
