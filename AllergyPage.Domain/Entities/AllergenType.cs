﻿using System.ComponentModel.DataAnnotations;

namespace AllergyPage.Domain.Entities
{
    public class AllergenType
    {
        [Key]
        public int CodeId { get; set; }
        public string CodeDesc { get; set; }
        public int CodeValue { get; set; }
        public string CodeText { get; set; }
        public int TypeId { get; set; }
    }
}
