﻿using AllergyPage.Domain.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace AllergyPage.Domain.Entities
{
    public class Client
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string CISId { get; set; }
        public DateTime DOB { get; set; }
        public Gender Gender { get; set; }
        public string Provider { get; set; }
        public Population Population { get; set; }
        public string AxisI { get; set; }
        public string AxisIDesc { get; set; }
    }
}
