﻿using AllergyPage.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AllergyPage.Persistence
{
    public static class DataSeeder
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Medication>().HasData(
                    new Medication() { Id = 10872, Name = "4-WAY MENTHOL", LexiDrugId = "d04186" },
                    new Medication() { Id = 4768, Name = "ABILIFY", LexiDrugId = "d04825" },
                    new Medication() { Id = 430, Name = "ACETAMINOPHEN", LexiDrugId = "d00049" },
                    new Medication() { Id = 1321, Name = "ACETAMINOPHEN", LexiDrugId = "d03423" },
                    new Medication() { Id = 1325, Name = "ACETAMINOPHEN", LexiDrugId = "d03428" },
                    new Medication() { Id = 1327, Name = "ACETAMINOPHEN", LexiDrugId = "d03431" },
                    new Medication() { Id = 15166, Name = "ADDERALL", LexiDrugId = "d04035" },
                    new Medication() { Id = 11582, Name = "ADVAIR HFA", LexiDrugId = "d04611" },
                    new Medication() { Id = 822, Name = "ALBUTEROL", LexiDrugId = "d00749" },
                    new Medication() { Id = 5761, Name = "AMBIEN", LexiDrugId = "d00910" },
                    new Medication() { Id = 510, Name = "AMITRIPTYLINE", LexiDrugId = "d00146" },
                    new Medication() { Id = 801, Name = "AMLODIPINE", LexiDrugId = "d00689" },
                    new Medication() { Id = 464, Name = "AMOXICILLIN", LexiDrugId = "d00088" },
                    new Medication() { Id = 551, Name = "Reveal Blood Glucose Test In Vitro Strip", LexiDrugId = "551" },
                    new Medication() { Id = 552, Name = "Phenylephrine Topical", LexiDrugId = "552" },
                    new Medication() { Id = 553, Name = "Reveal Methotrexate", LexiDrugId = "553" }
            );

            modelBuilder.Entity<AllergenReaction>().HasData(
                    new AllergenReaction() { CodeId = 614, CodeDesc = "Anaphylaxis", CodeValue = 39579001, TypeId = 64 },
                    new AllergenReaction() { CodeId = 615, CodeDesc = "Chest Pain", CodeValue = 139228007, TypeId = 64 },
                    new AllergenReaction() { CodeId = 616, CodeDesc = "Diarrhea", CodeValue = 139383004, TypeId = 64 },
                    new AllergenReaction() { CodeId = 617, CodeDesc = "Dizziness", CodeValue = 69096003, TypeId = 64 },
                    new AllergenReaction() { CodeId = 619, CodeDesc = "Headache", CodeValue = 139490008, TypeId = 64 },
                    new AllergenReaction() { CodeId = 618, CodeDesc = "Hives", CodeValue = 201272008, TypeId = 64 },
                    new AllergenReaction() { CodeId = 620, CodeDesc = "Irregular Heart Rate", CodeValue = 361138002, TypeId = 64 },
                    new AllergenReaction() { CodeId = 621, CodeDesc = "Itching", CodeValue = 32738000, TypeId = 64 },
                    new AllergenReaction() { CodeId = 641, CodeDesc = "N/A", CodeValue = null, TypeId = 64 },
                    new AllergenReaction() { CodeId = 622, CodeDesc = "Nausea", CodeValue = 139330007, TypeId = 64 },
                    new AllergenReaction() { CodeId = 629, CodeDesc = "Other", CodeValue = null, TypeId = 64 },
                    new AllergenReaction() { CodeId = 623, CodeDesc = "Photosensitivity", CodeValue = 90128006, TypeId = 64 },
                    new AllergenReaction() { CodeId = 624, CodeDesc = "Rash", CodeValue = 139684003, TypeId = 64 },
                    new AllergenReaction() { CodeId = 625, CodeDesc = "Respiratory Distress", CodeValue = 271825005, TypeId = 64 },
                    new AllergenReaction() { CodeId = 626, CodeDesc = "Sneezing", CodeValue = 76067001, TypeId = 64 },
                    new AllergenReaction() { CodeId = 627, CodeDesc = "Vomiting", CodeValue = 139337005, TypeId = 64 },
                    new AllergenReaction() { CodeId = 628, CodeDesc = "Wheezing", CodeValue = 139199004, TypeId = 64 }
            );

            modelBuilder.Entity<AllergenSeverity>().HasData(
                    new AllergenSeverity() { CodeId = 633, CodeDesc = "Life Threatening", CodeValue = 4, TypeId = 65 },
                    new AllergenSeverity() { CodeId = 630, CodeDesc = "Low", CodeValue = 1, TypeId = 65 },
                    new AllergenSeverity() { CodeId = 631, CodeDesc = "Moderate", CodeValue = 2, TypeId = 65 },
                    new AllergenSeverity() { CodeId = 635, CodeDesc = "N/A", CodeValue = 6, TypeId = 65 },
                    new AllergenSeverity() { CodeId = 632, CodeDesc = "Severe", CodeValue = 3, TypeId = 65 },
                    new AllergenSeverity() { CodeId = 634, CodeDesc = "Unknown", CodeValue = 5, TypeId = 65 }
            );

            modelBuilder.Entity<AllergenType>().HasData(
                    new AllergenType() { CodeId = 611, CodeDesc = "Allergen", CodeText = "Allergen", CodeValue = 3, TypeId = 63 },
                    new AllergenType() { CodeId = 612, CodeDesc = "Medication", CodeText = "Medication", CodeValue = 4, TypeId = 63 },
                    new AllergenType() { CodeId = 1267, CodeDesc = "No Known Allergies", CodeText = "No Known Allergies", CodeValue = 2, TypeId = 63 },
                    new AllergenType() { CodeId = 613, CodeDesc = "Other", CodeText = "Other", CodeValue = 1, TypeId = 63 }
            );

            modelBuilder.Entity<Allergen>().HasData(
                    new Allergen() { CodeId = 610, CodeDesc = "Aminoglycoside Antibiotics", CodeText = "Aminoglycoside Antibiotics", TypeId = 62 },
                    new Allergen() { CodeId = 581, CodeDesc = "Anesthesia", CodeText = "Anesthesia", TypeId = 62 },
                    new Allergen() { CodeId = 582, CodeDesc = "Animals(dander, skin saliva)", CodeText = "Animals(dander, skin saliva)", TypeId = 62 },
                    new Allergen() { CodeId = 583, CodeDesc = "Anti-seizure medication", CodeText = "Anti-seizure medication", TypeId = 62 },
                    new Allergen() { CodeId = 584, CodeDesc = "Aspirin", CodeText = "Aspirin", TypeId = 62 },
                    new Allergen() { CodeId = 585, CodeDesc = "Cephalosporins", CodeText = "Cephalosporins", TypeId = 62 },
                    new Allergen() { CodeId = 586, CodeDesc = "Dust", CodeText = "Dust", TypeId = 62 },
                    new Allergen() { CodeId = 587, CodeDesc = "Dye", CodeText = "Dye", TypeId = 62 },
                    new Allergen() { CodeId = 588, CodeDesc = "Eggs", CodeText = "Eggs", TypeId = 62 },
                    new Allergen() { CodeId = 589, CodeDesc = "Epinephrine", CodeText = "Epinephrine", TypeId = 62 },
                    new Allergen() { CodeId = 590, CodeDesc = "Fish", CodeText = "Fish", TypeId = 62 },
                    new Allergen() { CodeId = 580, CodeDesc = "Grass", CodeText = "Grass", TypeId = 62 },
                    new Allergen() { CodeId = 591, CodeDesc = "Ibuprofen", CodeText = "Ibuprofen", TypeId = 62 },
                    new Allergen() { CodeId = 592, CodeDesc = "Insect bite/sting/particles", CodeText = "Insect bite/sting/particles", TypeId = 62 },
                    new Allergen() { CodeId = 593, CodeDesc = "Lidocaine", CodeText = "Lidocaine", TypeId = 62 },
                    new Allergen() { CodeId = 594, CodeDesc = "Milk", CodeText = "Milk", TypeId = 62 },
                    new Allergen() { CodeId = 595, CodeDesc = "Mold", CodeText = "Mold", TypeId = 62 },
                    new Allergen() { CodeId = 596, CodeDesc = "Naproxen", CodeText = "Naproxen", TypeId = 62 },
                    new Allergen() { CodeId = 640, CodeDesc = "No Known Drug Allergies", CodeText = "NKDA", TypeId = 62 },
                    new Allergen() { CodeId = 597, CodeDesc = "Novocaine", CodeText = "Novocaine", TypeId = 62 },
                    new Allergen() { CodeId = 598, CodeDesc = "Nuts", CodeText = "Nuts", TypeId = 62 },
                    new Allergen() { CodeId = 599, CodeDesc = "Peanuts", CodeText = "Peanuts", TypeId = 62 },
                    new Allergen() { CodeId = 600, CodeDesc = "Penicillin", CodeText = "Penicillin", TypeId = 62 },
                    new Allergen() { CodeId = 601, CodeDesc = "Perfume", CodeText = "Perfume", TypeId = 62 },
                    new Allergen() { CodeId = 602, CodeDesc = "Plants", CodeText = "Plants", TypeId = 62 },
                    new Allergen() { CodeId = 603, CodeDesc = "Pollen", CodeText = "Pollen", TypeId = 62 },
                    new Allergen() { CodeId = 604, CodeDesc = "Shellfish", CodeText = "Shellfish", TypeId = 62 },
                    new Allergen() { CodeId = 605, CodeDesc = "Smoke", CodeText = "Smoke", TypeId = 62 },
                    new Allergen() { CodeId = 606, CodeDesc = "Soy", CodeText = "Soy", TypeId = 62 },
                    new Allergen() { CodeId = 607, CodeDesc = "Sulfonamides", CodeText = "Sulfonamides", TypeId = 62 },
                    new Allergen() { CodeId = 608, CodeDesc = "Trees", CodeText = "Trees", TypeId = 62 },
                    new Allergen() { CodeId = 609, CodeDesc = "Wheat", CodeText = "Wheat", TypeId = 62 }
            );

            modelBuilder.Entity<Client>().HasData(
                    new Client()
                    {
                        Id = 29355,
                        Name = "DOBIESZ",
                        CISId = "0742031237",
                        DOB = new DateTime(2011, 8, 3),
                        Gender = Domain.Enums.Gender.Male,
                        Provider = "Redcedar",
                        Population = Domain.Enums.Population.Children,
                        AxisI = "F25.0",
                        AxisIDesc = "Schizoaffective disorder, bipolar type"
                    }
            );

            modelBuilder.Entity<ClientAllergy>().HasData(

                    //new ClientAllergy() { Id = 13265, Note = "test again (edit 1)", CreateUser = "squinn", UpdateUser = "rkey", Deleted = false, AllergenId = 11582, ReactionId = 614, SeverityId = 631, AllergenTypeId = 612, DrugId = 11582, CreateDateWithTime = DateTime.Parse("2017-05-09T13:40:19.970"), UpdateDateWithTime = DateTime.Parse("2018-02-22T08:56:28.943"), ClientId = 29355 }
                    new ClientAllergy() { Id = 13398, Note = "Can't breath when pollen is high. (Edit 1)", CreateUser = "squinn", UpdateUser = "rkey", Deleted = false, AllergenId = 603, ReactionId = 614, SeverityId = 631, AllergenTypeId = 611, CreateDateWithTime = DateTime.Parse("2018-02-08T12:56:51.343"), UpdateDateWithTime = DateTime.Parse("2018-03-05T08:46:23.370"), ClientId = 29355 },
                    //new ClientAllergy() { Id = 13399, Note = "Just don't let him use these, OK??? (edit 2)", CreateUser = "squinn", UpdateUser = "rkey", Deleted = false, AllergenId = 31993, ReactionId = 614, SeverityId = 633, AllergenTypeId = 612, DrugId = 551, CreateDateWithTime = DateTime.Parse("2018-02-08T13:06:45.763"), UpdateDateWithTime = DateTime.Parse("2018-02-22T09:09:14.690"), ClientId = 29355 },
                    new ClientAllergy() { Id = 13400, Note = "This is a custom allergy.", CreateUser = "squinn", UpdateUser = string.Empty, Deleted = false, AllergenId = null, ReactionId = 614, SeverityId = 635, AllergenTypeId = 613, CreateDateWithTime = DateTime.Parse("2018-02-08T13:56:40.287"), ClientId = 29355 },
                    //new ClientAllergy() { Id = 13403, Note = "Adding this note after saving, again. (edit 2)", CreateUser = "squinn", UpdateUser = "rkey", Deleted = false, AllergenId = 37079, ReactionId = 614, SeverityId = 630, AllergenTypeId = 612, DrugId = 552, CreateDateWithTime = DateTime.Parse("2018-02-08T14:35:48.200"), UpdateDateWithTime = DateTime.Parse("2018-02-22T09:09:31.893"), ClientId = 29355 },
                    new ClientAllergy() { Id = 13405, Note = "Do no give this person near ANY sort of insect. (edited 4)", CreateUser = "squinn", UpdateUser = "rkey", Deleted = false, AllergenId = 592, ReactionId = 614, SeverityId = 630, AllergenTypeId = 611, CreateDateWithTime = DateTime.Parse("2018-02-08T15:35:55.847"), UpdateDateWithTime = DateTime.Parse("2018-02-22T08:55:11.960"), ClientId = 29355 },
                    //new ClientAllergy() { Id = 13406, Note = "Head literally explodes.", CreateUser = "squinn", UpdateUser = string.Empty, Deleted = false, AllergenId = 440, ReactionId = 614, SeverityId = 632, AllergenTypeId = 612, DrugId = 553, CreateDateWithTime = DateTime.Parse("2018-02-08T15:43:12.423"), ClientId = 29355 },
                    //new ClientAllergy() { Id = 13408, Note = "Do no give this patient Menthol.", CreateUser = "squinn", UpdateUser = string.Empty, Deleted = false, AllergenId = 10872, ReactionId = 614, SeverityId = 633, AllergenTypeId = 612, DrugId = 10872, CreateDateWithTime = DateTime.Parse("2018-02-08T15:45:46.500"), ClientId = 29355 },
                    new ClientAllergy() { Id = 13412, Note = "Gets stuffed up from grass, especially fresh grass.", CreateUser = "rkey", UpdateUser = string.Empty, Deleted = false, AllergenId = 580, ReactionId = 614, SeverityId = 630, AllergenTypeId = 611, CreateDateWithTime = DateTime.Parse("2018-03-05T09:15:50.660"), ClientId = 29355 },
                    new ClientAllergy() { Id = 13414, Note = "test duplication", CreateUser = "squinn", UpdateUser = string.Empty, Deleted = false, AllergenId = 586, ReactionId = 614, SeverityId = 633, AllergenTypeId = 611, CreateDateWithTime = DateTime.Parse("2018-05-03T11:35:54.973"), ClientId = 29355 }
                    //new ClientAllergy() { Id = 13415, Note = "testing allergies page for the loading thing", CreateUser = "rgarcia", UpdateUser = "rgarcia", Deleted = false, AllergenId = 430, ReactionId = 614, SeverityId = 635, AllergenTypeId = 612, DrugId = 430, CreateDateWithTime = DateTime.Parse("2018-08-02T10:07:48.457"), UpdateDateWithTime = DateTime.Parse("2018-08-09T15:46:22.430"), ClientId = 29355 },
                    //new ClientAllergy() { Id = 13416, Note = "test", CreateUser = "ksanowski", UpdateUser = "rgarcia", Deleted = false, AllergenId = 464, ReactionId = 614, SeverityId = 631, AllergenTypeId = 612, DrugId = 464, CreateDateWithTime = DateTime.Parse("2018-08-16T10:38:05.070"), UpdateDateWithTime = DateTime.Parse("2018-09-26T16:32:16.400"), ClientId = 29355 },
                    //new ClientAllergy() { Id = 13418, Note = "Test1235", CreateUser = "stener", UpdateUser = string.Empty, Deleted = false, AllergenId = 15242, ReactionId = 614, SeverityId = 633, AllergenTypeId = 612, DrugId = 1327, CreateDateWithTime = DateTime.Parse("2018-12-17T12:48:45.080"), ClientId = 29355 }
            );
        }
    }
}
