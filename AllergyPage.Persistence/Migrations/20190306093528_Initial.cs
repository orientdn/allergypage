﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AllergyPage.Persistence.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AllergenReactions",
                columns: table => new
                {
                    CodeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CodeDesc = table.Column<string>(nullable: true),
                    CodeValue = table.Column<int>(nullable: true),
                    TypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AllergenReactions", x => x.CodeId);
                });

            migrationBuilder.CreateTable(
                name: "Allergens",
                columns: table => new
                {
                    CodeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CodeDesc = table.Column<string>(nullable: true),
                    CodeText = table.Column<string>(nullable: true),
                    TypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Allergens", x => x.CodeId);
                });

            migrationBuilder.CreateTable(
                name: "AllergenSeveries",
                columns: table => new
                {
                    CodeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CodeDesc = table.Column<string>(nullable: true),
                    CodeValue = table.Column<int>(nullable: true),
                    TypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AllergenSeveries", x => x.CodeId);
                });

            migrationBuilder.CreateTable(
                name: "AllergenTypes",
                columns: table => new
                {
                    CodeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CodeDesc = table.Column<string>(nullable: true),
                    CodeValue = table.Column<int>(nullable: false),
                    CodeText = table.Column<string>(nullable: true),
                    TypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AllergenTypes", x => x.CodeId);
                });

            migrationBuilder.CreateTable(
                name: "Clients",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    CISId = table.Column<string>(nullable: true),
                    DOB = table.Column<DateTime>(nullable: false),
                    Gender = table.Column<int>(nullable: false),
                    Provider = table.Column<string>(nullable: true),
                    Population = table.Column<int>(nullable: false),
                    AxisI = table.Column<string>(nullable: true),
                    AxisIDesc = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Medications",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    LexiDrugId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Medications", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ClientAllergies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClientId = table.Column<int>(nullable: false),
                    AllergenId = table.Column<int>(nullable: true),
                    AllergenTypeId = table.Column<int>(nullable: true),
                    ReactionId = table.Column<int>(nullable: true),
                    SeverityId = table.Column<int>(nullable: true),
                    DrugId = table.Column<int>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    CreateUser = table.Column<string>(nullable: true),
                    UpdateUser = table.Column<string>(nullable: true),
                    CreateDateWithTime = table.Column<DateTime>(nullable: false),
                    UpdateDateWithTime = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientAllergies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientAllergies_Allergens_AllergenId",
                        column: x => x.AllergenId,
                        principalTable: "Allergens",
                        principalColumn: "CodeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientAllergies_AllergenTypes_AllergenTypeId",
                        column: x => x.AllergenTypeId,
                        principalTable: "AllergenTypes",
                        principalColumn: "CodeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientAllergies_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClientAllergies_Medications_DrugId",
                        column: x => x.DrugId,
                        principalTable: "Medications",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientAllergies_AllergenReactions_ReactionId",
                        column: x => x.ReactionId,
                        principalTable: "AllergenReactions",
                        principalColumn: "CodeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClientAllergies_AllergenSeveries_SeverityId",
                        column: x => x.SeverityId,
                        principalTable: "AllergenSeveries",
                        principalColumn: "CodeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AllergenReactions",
                columns: new[] { "CodeId", "CodeDesc", "CodeValue", "TypeId" },
                values: new object[,]
                {
                    { 618, "Hives", 201272008, 64 },
                    { 614, "Anaphylaxis", 39579001, 64 },
                    { 615, "Chest Pain", 139228007, 64 },
                    { 616, "Diarrhea", 139383004, 64 },
                    { 617, "Dizziness", 69096003, 64 },
                    { 619, "Headache", 139490008, 64 },
                    { 620, "Irregular Heart Rate", 361138002, 64 },
                    { 621, "Itching", 32738000, 64 },
                    { 641, "N/A", null, 64 },
                    { 622, "Nausea", 139330007, 64 },
                    { 629, "Other", null, 64 },
                    { 623, "Photosensitivity", 90128006, 64 },
                    { 624, "Rash", 139684003, 64 },
                    { 625, "Respiratory Distress", 271825005, 64 },
                    { 627, "Vomiting", 139337005, 64 },
                    { 628, "Wheezing", 139199004, 64 },
                    { 626, "Sneezing", 76067001, 64 }
                });

            migrationBuilder.InsertData(
                table: "AllergenSeveries",
                columns: new[] { "CodeId", "CodeDesc", "CodeValue", "TypeId" },
                values: new object[,]
                {
                    { 633, "Life Threatening", 4, 65 },
                    { 630, "Low", 1, 65 },
                    { 631, "Moderate", 2, 65 },
                    { 635, "N/A", 6, 65 },
                    { 632, "Severe", 3, 65 },
                    { 634, "Unknown", 5, 65 }
                });

            migrationBuilder.InsertData(
                table: "AllergenTypes",
                columns: new[] { "CodeId", "CodeDesc", "CodeText", "CodeValue", "TypeId" },
                values: new object[,]
                {
                    { 611, "Allergen", "Allergen", 3, 63 },
                    { 612, "Medication", "Medication", 4, 63 },
                    { 1267, "No Known Allergies", "No Known Allergies", 2, 63 },
                    { 613, "Other", "Other", 1, 63 }
                });

            migrationBuilder.InsertData(
                table: "Allergens",
                columns: new[] { "CodeId", "CodeDesc", "CodeText", "TypeId" },
                values: new object[,]
                {
                    { 609, "Wheat", "Wheat", 62 },
                    { 608, "Trees", "Trees", 62 },
                    { 610, "Aminoglycoside Antibiotics", "Aminoglycoside Antibiotics", 62 },
                    { 606, "Soy", "Soy", 62 },
                    { 581, "Anesthesia", "Anesthesia", 62 },
                    { 582, "Animals(dander, skin saliva)", "Animals(dander, skin saliva)", 62 },
                    { 583, "Anti-seizure medication", "Anti-seizure medication", 62 },
                    { 584, "Aspirin", "Aspirin", 62 },
                    { 585, "Cephalosporins", "Cephalosporins", 62 },
                    { 586, "Dust", "Dust", 62 },
                    { 607, "Sulfonamides", "Sulfonamides", 62 },
                    { 588, "Eggs", "Eggs", 62 },
                    { 589, "Epinephrine", "Epinephrine", 62 },
                    { 590, "Fish", "Fish", 62 },
                    { 580, "Grass", "Grass", 62 },
                    { 591, "Ibuprofen", "Ibuprofen", 62 },
                    { 592, "Insect bite/sting/particles", "Insect bite/sting/particles", 62 },
                    { 593, "Lidocaine", "Lidocaine", 62 },
                    { 587, "Dye", "Dye", 62 },
                    { 595, "Mold", "Mold", 62 },
                    { 594, "Milk", "Milk", 62 },
                    { 605, "Smoke", "Smoke", 62 },
                    { 604, "Shellfish", "Shellfish", 62 },
                    { 602, "Plants", "Plants", 62 },
                    { 601, "Perfume", "Perfume", 62 },
                    { 600, "Penicillin", "Penicillin", 62 },
                    { 603, "Pollen", "Pollen", 62 },
                    { 599, "Peanuts", "Peanuts", 62 },
                    { 598, "Nuts", "Nuts", 62 },
                    { 597, "Novocaine", "Novocaine", 62 },
                    { 640, "No Known Drug Allergies", "NKDA", 62 },
                    { 596, "Naproxen", "Naproxen", 62 }
                });

            migrationBuilder.InsertData(
                table: "Clients",
                columns: new[] { "Id", "AxisI", "AxisIDesc", "CISId", "DOB", "Gender", "Name", "Population", "Provider" },
                values: new object[] { 29355, "F25.0", "Schizoaffective disorder, bipolar type", "0742031237", new DateTime(2011, 8, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, "DOBIESZ", 0, "Redcedar" });

            migrationBuilder.InsertData(
                table: "Medications",
                columns: new[] { "Id", "LexiDrugId", "Name" },
                values: new object[,]
                {
                    { 822, "d00749", "ALBUTEROL" },
                    { 464, "d00088", "AMOXICILLIN" },
                    { 801, "d00689", "AMLODIPINE" },
                    { 510, "d00146", "AMITRIPTYLINE" },
                    { 5761, "d00910", "AMBIEN" },
                    { 551, "551", "Reveal Blood Glucose Test In Vitro Strip" },
                    { 11582, "d04611", "ADVAIR HFA" },
                    { 10872, "d04186", "4-WAY MENTHOL" },
                    { 1327, "d03431", "ACETAMINOPHEN" },
                    { 1325, "d03428", "ACETAMINOPHEN" },
                    { 1321, "d03423", "ACETAMINOPHEN" },
                    { 430, "d00049", "ACETAMINOPHEN" },
                    { 4768, "d04825", "ABILIFY" },
                    { 552, "552", "Phenylephrine Topical" },
                    { 15166, "d04035", "ADDERALL" },
                    { 553, "553", "Reveal Methotrexate" }
                });

            migrationBuilder.InsertData(
                table: "ClientAllergies",
                columns: new[] { "Id", "AllergenId", "AllergenTypeId", "ClientId", "CreateDateWithTime", "CreateUser", "Deleted", "DrugId", "Note", "ReactionId", "SeverityId", "UpdateDateWithTime", "UpdateUser" },
                values: new object[,]
                {
                    { 13398, 603, 611, 29355, new DateTime(2018, 2, 8, 12, 56, 51, 343, DateTimeKind.Unspecified), "squinn", false, null, "Can't breath when pollen is high. (Edit 1)", 614, 631, new DateTime(2018, 3, 5, 8, 46, 23, 370, DateTimeKind.Unspecified), "rkey" },
                    { 13400, null, 613, 29355, new DateTime(2018, 2, 8, 13, 56, 40, 287, DateTimeKind.Unspecified), "squinn", false, null, "This is a custom allergy.", 614, 635, null, "" },
                    { 13405, 592, 611, 29355, new DateTime(2018, 2, 8, 15, 35, 55, 847, DateTimeKind.Unspecified), "squinn", false, null, "Do no give this person near ANY sort of insect. (edited 4)", 614, 630, new DateTime(2018, 2, 22, 8, 55, 11, 960, DateTimeKind.Unspecified), "rkey" },
                    { 13412, 580, 611, 29355, new DateTime(2018, 3, 5, 9, 15, 50, 660, DateTimeKind.Unspecified), "rkey", false, null, "Gets stuffed up from grass, especially fresh grass.", 614, 630, null, "" },
                    { 13414, 586, 611, 29355, new DateTime(2018, 5, 3, 11, 35, 54, 973, DateTimeKind.Unspecified), "squinn", false, null, "test duplication", 614, 633, null, "" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ClientAllergies_AllergenId",
                table: "ClientAllergies",
                column: "AllergenId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAllergies_AllergenTypeId",
                table: "ClientAllergies",
                column: "AllergenTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAllergies_ClientId",
                table: "ClientAllergies",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAllergies_DrugId",
                table: "ClientAllergies",
                column: "DrugId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAllergies_ReactionId",
                table: "ClientAllergies",
                column: "ReactionId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientAllergies_SeverityId",
                table: "ClientAllergies",
                column: "SeverityId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClientAllergies");

            migrationBuilder.DropTable(
                name: "Allergens");

            migrationBuilder.DropTable(
                name: "AllergenTypes");

            migrationBuilder.DropTable(
                name: "Clients");

            migrationBuilder.DropTable(
                name: "Medications");

            migrationBuilder.DropTable(
                name: "AllergenReactions");

            migrationBuilder.DropTable(
                name: "AllergenSeveries");
        }
    }
}
