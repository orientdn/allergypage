﻿using AllergyPage.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace AllergyPage.Persistence
{
    public class AllergyPageDbContext : DbContext, IDbContext
    {
        public AllergyPageDbContext(DbContextOptions<AllergyPageDbContext> options) : base(options) { }

        public DbSet<Client> Clients { get; set; }
        public DbSet<ClientAllergy> ClientAllergies { get; set; }
        public DbSet<Allergen> Allergens { get; set; }
        public DbSet<AllergenReaction> AllergenReactions { get; set; }
        public DbSet<AllergenSeverity> AllergenSeveries { get; set; }
        public DbSet<AllergenType> AllergenTypes { get; set; }
        public DbSet<Medication> Medications { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Seed();
        }
    }
}
