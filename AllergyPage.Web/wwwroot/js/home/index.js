﻿
var AllergyInputController = function () {
    this.initialize = function () {
        loadReaction();
        loadSeverity();
        loadAllergenType();
        registerEvents();
    }

    function registerEvents() {
        $('#btnSave').on('click', function (e) {
            saveAllergy(e);
        });

        $('#btnReset').on('click', function (e) {
            resetFormData();
        });

        $('#ddlAllergenType').on('change', function () {
            textAllergenType = $("#ddlAllergenType option:selected").text();
            console.log(textAllergenType);
            if (textAllergenType == "Allergen") {
                $("#allergen").show()
                $("#prescription").hide()
                loadAllergen();
            }
            else if (textAllergenType == "Medication") {
                $("#allergen").hide()
                $("#prescription").show()
            }
            else {
                $("#allergen").hide()
                $("#prescription").hide()
            }


        });
    }

    function saveAllergy(e) {
        e.preventDefault();
        var clientId = 29355;
        var allergenId = $('#ddlAllergen').val();
        var allergenTypeId = $('#ddlAllergenType').val();
        var reactionId = $('#ddlReacttion').val();
        var severityId = $('#ddlSeverity').val();
        var noteMessage = $('#txtNoteMessage').val();

        var dataPush = {
            clientId: clientId,
            allergenId: allergenId,
            allergenTypeId: allergenTypeId,
            reactionId: reactionId,
            severityId: severityId,
            drugId: null,
            note: noteMessage,
        }

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/api/AllergyHistory/add",
            data: JSON.stringify(dataPush),
            dataType: "json",
        }).done(function (data) {
            debugger
            resetFormData();
            window.table && window.table.ajax.reload();
            }).fail(function (err) {
                debugger
                console.log(err)
            resetFormData();
            window.table && window.table.ajax.reload();
        }).always(function () {
            resetFormData();
            window.table && window.table.ajax.reload();
        });
    }

    function resetFormData() {
        $('#txtNoteMessage').val('');
        $('#ddlReacttion').prop('selectedIndex', 0);
        $('#ddlSeverity').prop('selectedIndex', 0);
        $('#ddlAllergenType').prop('selectedIndex', 0);
        $('#ddlAllergen').prop('selectedIndex', 0);
    }

    function loadReaction() {
        $.ajax({
            type: 'GET',
            url: '/api/AllergenReaction',
            dataType: 'json',
            success: function (response) {
                var render = "<option value=''>--Please select an option--</option>";
                $.each(response, function (i, item) {
                    render += "<option value='" + item.codeId + "'>" + item.codeDesc + "</option>"
                });
                $('#ddlReacttion').html(render);
            },
            error: function (status) {
                console.log(status);
            }
        });
    }

    function loadSeverity() {
        $.ajax({
            type: 'GET',
            url: '/api/allergenSeverity',
            dataType: 'json',
            success: function (response) {
                var render = "<option value=''>--Please select an option--</option>";
                $.each(response, function (i, item) {
                    render += "<option value='" + item.codeId + "'>" + item.codeDesc + "</option>"
                });
                $('#ddlSeverity').html(render);
            },
            error: function (status) {
                console.log(status);
            }
        });
    }

    function loadAllergenType() {
        $.ajax({
            type: 'GET',
            url: '/api/AllergenType',
            dataType: 'json',
            success: function (response) {
                var render = "<option value=''>--Please select an option--</option>";
                $.each(response, function (i, item) {
                    render += "<option value='" + item.codeId + "'>" + item.codeDesc + "</option>"
                });
                $('#ddlAllergenType').html(render);
            },
            error: function (status) {
                console.log(status);
            }
        });
    }
    function loadAllergen() {
        $.ajax({
            type: 'GET',
            url: '/api/Allergen',
            dataType: 'json',
            success: function (response) {
                var render = "<option value=''>--Please select an option--</option>";
                $.each(response, function (i, item) {
                    render += "<option value='" + item.codeId + "'>" + item.codeDesc + "</option>"
                });
                $('#ddlAllergen').html(render);
            },
            error: function (status) {
                console.log(status);
            }
        });
    }
}