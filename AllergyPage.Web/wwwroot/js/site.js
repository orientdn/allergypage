﻿$(document).ready(function () {
    let columns = [
        {
            title: 'Patient',
            name: "Patient",
            data: "client.name",
            defaultContent: "",
            orderable: false,
            className: ""
        },
        {
            title: 'Type',
            name: "Type",
            data: "allergenType.codeText",
            defaultContent: "",
            orderable: false,
            className: ""
        },
        {
            title: 'Allergen',
            name: "Allergen",
            data: "allergen.codeText",
            defaultContent: "",
            orderable: false,
            className: ""
        },
        {
            title: 'Reaction',
            name: "Reaction",
            data: "reaction.codeDesc",
            defaultContent: "",
            orderable: false,
            className: ""
        },
        {
            title: 'Serverity',
            name: "Serverity",
            data: "severity.codeDesc",
            defaultContent: "",
            orderable: false,
            className: ""
        },
        {
            title: 'Notes',
            name: "Notes",
            data: "note",
            defaultContent: "",
            orderable: false,
            className: ""
        },
        {
            title: 'Create Info',
            name: "CreateInfo",
            data: function (data) {
                return `Created on ${moment(data.createDateWithTime).format('L')} by <b>${data.createUser || "unknow"}</b>`
            },
            defaultContent: "",
            orderable: false,
            className: ""
        },
        {
            title: 'Update Info',
            name: "UpdateInfo",
            data: "updateDateWithTime",
            data: function (data) {
                if (!data.updateUser && !data.updateDateWithTime) {
                    return ''
                }
                return `Updated on ${moment(data.updateDateWithTime).format('L')} by <b>${data.updateUser || "unknow"}</b>`
            },
            defaultContent: "",
            orderable: false,
            className: ""
        },
        {
            title: 'Action',
            name: "Action",
            data: function (data) {
                return `<select>
                    <option>Edit</option>
                </select>`
            },
            defaultContent: "",
            orderable: false,
            className: ""
        }
    ]

    window.table = $('#table').DataTable(
        {
            columns: columns,
            serverSide: true,
            searching: true,
            paging: true,
            info: true,
            sort: false,
            dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            buttons: [
                //'print',
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i> Print',
                    title: $('h1').text(),
                    exportOptions: {
                        columns: ':not(.no-print)'
                    },
                    footer: true,
                    autoPrint: true,
                    orientation: 'landscape',
                }
            ],
            ajax: {
                type: "POST",
                url: '/api/AllergyHistory',
                contentType: "application/json",
                data: function (data) {
                    console.log(data)
                    let customs = {
                        active: $("#select-active").val(),
                        timeRange: $("#select-time-range").val()
                    }
                    data.customs = customs
                    return JSON.stringify(data);
                }
            },
        }
    );

    $("#btn-print").click(() => {
        $(".buttons-print").trigger('click');
    })

    $("#tb-search").change((e) => {
        debugger
    })

    $("#select-active, #select-time-range").change(() => {
        table.ajax.reload();
    })
});