﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AllergyPage.Application.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AllergyPage.Web.Apis
{
    [Route("api/[controller]")]
    [ApiController]
    public class AllergenSeverityController : ControllerBase
    {
        private readonly IAllergenService _allergenService;

        public AllergenSeverityController(IAllergenService allergenService)
        {
            _allergenService = allergenService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var result = _allergenService.GetAllSeverity();
            return Ok(result);
        }
    }
}
