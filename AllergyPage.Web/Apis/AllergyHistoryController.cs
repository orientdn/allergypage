﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AllergyPage.Application.Dtos;
using AllergyPage.Application.Services;
using AllergyPage.Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AllergyPage.Web.Apis
{
    [Route("api/[controller]")]
    [ApiController]
    public class AllergyHistoryController : ControllerBase
    {
        private readonly IClientService _clientService;

        public AllergyHistoryController(IClientService clientService)
        {
            _clientService = clientService;
        }

        [HttpPost]
        public IActionResult Get([FromBody]QueryDataDto query)
        {
            var result = _clientService.GetAllergyHistories(query);
            return Ok(result);
        }

        // POST: api/AllergyHistory
        [HttpPost("add")]
        public IActionResult Post(SaveDataVM saveDataVM)
        {
            ClientAllergy clientAllergy = new ClientAllergy();
            clientAllergy.ClientId = saveDataVM.ClientId;
            clientAllergy.AllergenTypeId = saveDataVM.AllergenTypeId;
            clientAllergy.AllergenId = saveDataVM.AllergenId;
            clientAllergy.ReactionId = saveDataVM.ReactionId;
            clientAllergy.SeverityId = saveDataVM.SeverityId;
            clientAllergy.DrugId = saveDataVM.DrugId;

            clientAllergy.Note = saveDataVM.Note;
            clientAllergy.CreateDateWithTime = DateTime.UtcNow;
            clientAllergy.CreateUser = "squinn";

            _clientService.saveData(clientAllergy);

            return Ok();
        }
    }
}
