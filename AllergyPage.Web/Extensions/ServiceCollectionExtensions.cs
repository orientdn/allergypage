﻿using AllergyPage.Application.Interfaces;
using AllergyPage.Application.Services;
using AllergyPage.Persistence;
using AllergyPage.Persistence.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AllergyPage.Web.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddApplicationServices(this IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddScoped<IDbContext, AllergyPageDbContext>();
            services.AddScoped(typeof(IRepository<>), typeof(GenericRepository<>));

            #region Services

            services.AddScoped<IAllergenService, AllergenService>();
            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IMedicationService, MedicationService>();

            #endregion
        }
    }
}
